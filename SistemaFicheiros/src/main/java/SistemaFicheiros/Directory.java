/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemaFicheiros;

import java.util.*;

/**
 *
 * @author ines-
 */
public class Directory extends INode {

    private HashMap<String, Directory> children;
    private HashMap<String, File> files;

    public Directory(String name) {
        super(name);
        children = new HashMap<>();
        files = new HashMap<>();
    }

    @Override
    public boolean isDirectory() {
        return true;
    }

    public boolean contains(String nodeName) {
        return children.containsKey(nodeName);
    }

    public List<String> listDirectory(Directory root) {
        List<String> objects = new ArrayList<>();
        if (!root.files.isEmpty()) {
            root.files.keySet().forEach((file) -> {
                objects.add(file);
            });
        }
        if (!root.children.isEmpty()) {
            root.children.keySet().forEach((path) -> {
                objects.add(path);
            });
        }
        Collections.sort(objects);
        return objects;
    }

    public void createDirectory(String path) {
        String[] paths = path.split("\\\\");
        Directory p = new Directory("\\\\");
        for (String subPath : paths) {
            if (!p.children.containsKey(subPath)) {
                Directory d = new Directory(subPath);
                p.children.put(subPath, d);
            }
            p = p.children.get(subPath);

        }
    }

    public void copyFile(String pathFile, String pathNewFile) {
        String[] paths = pathFile.split("\\\\");
        File file = files.get(pathFile);

        File fileAux = new File(pathNewFile, file.getContent());
        System.out.println(fileAux);
    }

    public void moveFile(String pathFile, String pathNewFile) {
        copyFile(pathFile, pathNewFile);
        removeFile(pathFile);
    }

    /**
     * Remove an empty directory.
     *
     * @param path
     */
    public void removeDirectory(String path) {
        String[] paths = path.split("\\\\");
        Directory p = null;
        for (int i = paths.length - 1; i >= 0; i--) {
            if (p.children.get(paths[i]) == null && p.files.get(paths[i]) == null) {
                children.remove(paths[i]);
            } else {
                System.out.println("removeDirectory:" + path + ": No such file or directory");
            }
        }
    }

    /**
     * Remove a file.
     *
     * @param path
     */
    public void removeFile(String path) {
        String[] paths = path.split("\\\\");
        if (files.get(paths[paths.length - 1]) != null) {
            files.remove(paths[paths.length - 1]);
        } else {
            System.out.println("removeFile:" + path + ": No such file or directory");
        }
    }

    @Override
    public long getLength() {
        long length = 0;
        for (Directory node : children.values()) {
            length += node.getLength();
        }
        return length;

    }

    @Override
    public String toString() {
        String[] names = this.getName().split("\\\\");
        return "Root: " + this.getName()
                + ", \nName: '" + names[names.length - 1] + '\''
                + ", \nCreated: " + this.getCreated()
                + ", \nLast Updated: " + this.getLastUpdated()
                + ", \nLast Accessed: " + this.getLastAccessed();

    }
}

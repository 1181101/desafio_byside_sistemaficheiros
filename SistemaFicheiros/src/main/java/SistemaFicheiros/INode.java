/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemaFicheiros;

import java.time.LocalDateTime;

/**
 *
 * @author ines-
 */
public abstract class INode {

    private Directory root;
    private String name;
    private LocalDateTime created;
    private LocalDateTime lastUpdated;
    private LocalDateTime lastAccessed;

    public INode(String name) {
        this.name = name;
        this.created = LocalDateTime.now();
        this.lastUpdated = LocalDateTime.now();
        this.lastAccessed = LocalDateTime.now();
    }

    public abstract boolean isDirectory();

    public abstract long getLength();

    // methods to get the name, rename, etc..
    public String getName() {
        return this.name;
    }

    public void rename(String newName) {
        if (newName != null) {
            this.name = newName;
        }
    }

    public INode getRoot() {
        return this.root;
    }

    public void setRoot(Directory root) {
        this.root = root;
    }

    public String getPath() {
        if (this.root != null) {
            return this.root.getPath() + "/" + this.name;
        } else {
            return this.name;
        }
    }

    public LocalDateTime getCreated() {
        return this.created;
    }

    public LocalDateTime getLastUpdated() {
        return this.lastUpdated;
    }

    public LocalDateTime getLastAccessed() {
        return this.lastAccessed;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public void setLastAccessed(LocalDateTime lastAccessed) {
        this.lastAccessed = lastAccessed;
    }

    @Override
    public abstract String toString();

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemaFicheiros;

import java.time.LocalDateTime;

/**
 *
 * @author ines-
 */
public class File extends INode {

    private String content;
    private boolean read;
    private boolean write;
    private boolean execute;

    public File(String name, String content) {
        super(name);
        this.content = content;
        this.read = true;
        this.write = false;
        this.execute = false;
    }

    public File(String name) {
        super(name);
        this.content = null;
        this.read = true;
        this.write = false;
        this.execute = false;
    }

    public File(String name, String content, boolean read, boolean write, boolean execute) {
        super(name);
        this.content = content;
        this.read = read;
        this.write = write;
        this.execute = execute;
    }

    @Override
    public boolean isDirectory() {
        return false;
    }

    /* public String getName(){
        String[] paths = this.getName().
    }*/
    public String getContent() {
        return this.content;
    }

    public void setContent(String newContent) {
        this.setLastUpdated(LocalDateTime.now());
        this.setLastAccessed(LocalDateTime.now());
        this.content = newContent;
    }

    public void addContent(String newContent) {
        if (content != null) {
            this.setLastUpdated(LocalDateTime.now());
            this.setLastAccessed(LocalDateTime.now());
            this.content = this.content + newContent;
        }
    }

    public void deleteContent() {
        this.setLastUpdated(LocalDateTime.now());
        this.setLastAccessed(LocalDateTime.now());
        this.content = null;
    }

    @Override
    public long getLength() {
        if (content != null) {
            return content.getBytes().length;
        }
        return 0;
    }

    public boolean canRead() {
        return this.read;
    }

    public boolean canWrite() {
        return this.write;
    }

    public boolean canExecute() {
        return this.execute;
    }

    public boolean setReadable(boolean readable) {
        return this.read = readable;
    }

    public boolean setWritable(boolean writable) {
        return this.write = writable;
    }

    public boolean setExecutable(boolean executable) {
        return this.execute = executable;
    }

    @Override
    public String toString() {
        String[] names = this.getName().split("\\\\");
        return "Root: " + this.getName()
                + ", \nName: '" + names[names.length - 1] + '\''
                + ", \nCreated: " + this.getCreated()
                + ", \nLast Updated: " + this.getLastUpdated()
                + ", \nLast Accessed: " + this.getLastAccessed();

    }

}
